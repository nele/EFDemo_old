﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*List<Expression<Func<string, bool>>> ListFunc = new List<Expression<Func<string, bool>>>();
            Expression<Func<string, bool>> func = name => true;

            ListFunc.Add(func);
            ListFunc.Add(func);
            ListFunc.Add(func);

            Expression<Func<string, bool>> be = null;
            foreach (var expr in ListFunc)
            {
                if (be == null)
                {
                    be = expr;
                    //cev.Parameters = expr.Parameters;
                }
                else
                {
                    List<ParameterExpression> parameters = new List<ParameterExpression>();
                    parameters.AddRange(expr.Parameters);
                    Expression left = expr.Update(expr.Body, parameters).Body;
                    Expression right = be.Body;
                    Expression and = Expression.AndAlso(left, right);
                    be = Expression.Lambda<Func<string, bool>>(and, parameters);
                }
            }

            Console.ReadLine();
            */

            List<WhereFilter> filters = new List<WhereFilter>();

            filters.Add(new WhereFilter { Key = "Id", Value = "1", Contrast = ">" });
            filters.Add(new WhereFilter { Key = "Username", Value = "ne", Contrast = "like" });
            filters.Add(new WhereFilter { Key = "CrateDate", Value = "2016-5-19", Contrast = ">" });

            List<User> UserList = new List<User>();
            UserList.Add(new User { Id = 1, CrateDate = DateTime.Now, Username = "nele" });
            UserList.Add(new User { Id = 2, CrateDate = DateTime.Now, Username = "nele2" });
            UserList.Add(new User { Id = 3, CrateDate = DateTime.Now, Username = "nele3" });
            UserList.Add(new User { Id = 4, CrateDate = DateTime.Now, Username = "nele4" });
            UserList.Add(new User { Id = 5, CrateDate = DateTime.Now, Username = "nele5" });
            UserList.Add(new User { Id = 6, CrateDate = DateTime.Now, Username = "nele6" });

            var users = UserList.AsQueryable();

            IQueryable<User> query = users.GenerateFilter(filters);
        }
    }
}
