﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo3
{
	class ComposeExpressionVisitor : ExpressionVisitor
	{
		public ReadOnlyCollection<ParameterExpression> Parameters { get; set; }

		//public Expression ChangeParameter(Expression target)
		//{

		//}

		protected override Expression VisitParameter(ParameterExpression node)
		{
			foreach (var item in this.Parameters)
			{
				if (item.Type == node.Type)
				{
					return item;
				}
			}
			throw new ApplicationException("找不到参数: " + node.Name + "<" + node.Type.FullName + ">");
		}
	}
}
