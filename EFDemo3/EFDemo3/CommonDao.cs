﻿/*
 * Model: 
 * Desctiption: 
 * Author: 陈飞
 * Created: 2016/5/17 17:39:18 
 * Copyright：武汉中科通达高新技术股份有限公司
 */

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Linq.Expressions;

namespace EFDemo3
{
	public class CommonDao : IDisposable
	{
		public string ConnectionString { get; set; }

		public CommonDao(string s)
		{
			this.ConnectionString = s;
		}

		
		public IList<TEntity> Query<TEntity, TKey>(EfDbQuery<TEntity, TKey> qry) where TEntity : Book
		{
			using (CommContext<Book> ctx = new CommContext<Book>(ConnectionString))
			{
				ctx.ModelBuilder = new DbModelBuilder();
				ctx.ModelBuilder.Configurations.Add<TEntity>(Activator.CreateInstance<EntityTypeConfiguration<TEntity>>());
				EfDbQuery<TEntity, TKey> query = qry as EfDbQuery<TEntity, TKey>;
				Expression<Func<TEntity, bool>> func = query.ToQueryFunc();
				var list = ctx.Set<TEntity>().Where<TEntity>(func).ToList<TEntity>();
				return list;
			}
		}

		public void Dispose()
		{
		}
	}

	public class EfDbQuery<TEntity, TKey> where TEntity : class
	{
		private IList<Expression<Func<TEntity, bool>>> _conditions;

		public Expression<Func<TEntity, bool>> ToQueryFunc()
		{

			if (_conditions == null)
			{
				return (item) => true;
			}

			ComposeExpressionVisitor cev = new ComposeExpressionVisitor();
			Expression<Func<TEntity, bool>> be = null;
			foreach (var expr in _conditions)
			{
				if (be == null)
				{
					be = expr;
					cev.Parameters = expr.Parameters;
				}
				else
				{
					List<ParameterExpression> paramters = new List<ParameterExpression>();
					paramters.AddRange(expr.Parameters);
					Expression left = cev.Visit(expr.Body);
					Expression right = cev.Visit(be.Body);
					Expression and = Expression.AndAlso(left, right);
					be = Expression.Lambda<Func<TEntity, bool>>(and, cev.Parameters);
				}
			}

			return (Expression<Func<TEntity, bool>>)be;
		}

		public Expression<Func<TEntity, bool>> ToQueryFunc2()
		{

			if (_conditions == null)
			{
				return (item) => true;
			}

			//ComposeExpressionVisitor cev = new ComposeExpressionVisitor();
			Expression<Func<TEntity, bool>> be = null;
			foreach (var expr in _conditions)
			{
				if (be == null)
				{
					be = expr;
					//cev.Parameters = expr.Parameters;
				}
				else
				{
					List<ParameterExpression> parameters = new List<ParameterExpression>();
					parameters.AddRange(expr.Parameters);
					Expression left = expr.Update(expr.Body, parameters).Body;
					Expression right = be.Body;
					Expression and = Expression.AndAlso(left, right);
					be = Expression.Lambda<Func<TEntity, bool>>(and, parameters);
				}
			}

			return (Expression<Func<TEntity, bool>>)be;
		}

		public void AddFunc(Expression<Func<TEntity, bool>> func)
		{
			if (this._conditions == null)
			{
				this._conditions = new List<Expression<Func<TEntity, bool>>>();
			}
			this._conditions.Add(func);
		}
	}
}
