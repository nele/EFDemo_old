﻿/*
 * Model: 
 * Desctiption: 
 * Author: 陈飞
 * Created: 2016/4/12 17:10:20 
 * Copyright：武汉中科通达高新技术股份有限公司
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFDemo3
{
	[Table("BOOK", Schema = "CHENF")]
	public class Book
	{
		private string _id;
		
		//[Column("ID")]
		public string Id {
			get { return _id; }
			set { _id = value; }
		}

		//[Column("TITLE")]
		public string Title { get; set; }

		//[Column("UNITPRICE")]
		public decimal UnitPrice { get; set; }

		public Category Category { get; set; }
	}
}
