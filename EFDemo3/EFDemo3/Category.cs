﻿/*
 * Model: 
 * Desctiption: 
 * Author: 陈飞
 * Created: 2016/4/13 8:36:43 
 * Copyright：武汉中科通达高新技术股份有限公司
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFDemo3
{
	[Table("Category", Schema = "CHENF")]
	public class Category
	{
		public string Id { get; set; }

		public string Name { get; set; }

		public string Remark { get; set; }
	}
}
