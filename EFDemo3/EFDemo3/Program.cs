﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo3
{
	//class CommonDao : IDisposable
	//{
	//	public string connstr;

	//	public CommonDao(string connstr)
	//	{
	//		this.connstr = connstr;
	//	}
	//	class GDC<T> : DbContext where T : class
	//	{
	//		public GDC()
	//		{

	//		}

	//		protected override void OnModelCreating(DbModelBuilder modelBuilder)
	//		{
	//			modelBuilder.HasDefaultSchema("CHENF");
	//		}
	//		public GDC(string connstr) : base(connstr)
	//		{
	//			this.Database.Log = new Action<string>((s) =>
	//			{
	//				Console.ForegroundColor = ConsoleColor.Green;
	//				Console.WriteLine(s);
	//				Console.ResetColor();
	//			});
	//		}

	//		public DbSet<T> Items { get; set; }
	//	}

	//	public IList<T> Query<T>() where T : class
	//	{
	//		using (GDC<T> ctx = new GDC<T>(connstr))
	//		{
	//			return ctx.Items.ToList();
	//		}
	//	}

	//	public int Insert<T>(T item) where T : class
	//	{
	//		using
	//			(GDC<T> ctx = new GDC<T>(connstr))
	//		{
	//			ctx.Items.Add(item);
	//			int count = ctx.SaveChanges();
	//			return count;
	//		}
	//	}

	//	public void Dispose()
	//	{

	//	}
	//}

   

	class Program
    {
        private static Expression<Func<Book, bool>> GetFunc()
        {
            return (Book b) => b.Title.Contains("C");
        }

        static void Main(string[] args)
		{
			using (CommonDao bc = new CommonDao("OraString"))
			{

				//bc.Database.Log = new Action<string>((s) => {
				//	Console.ForegroundColor = ConsoleColor.Green;
				//	Console.WriteLine(s);
				//	Console.ResetColor();
				//});

				Book book = new Book
				{
					Id = Guid.NewGuid().ToString("N"),
					Title = "C# 高级编程",
					Category = null,
					UnitPrice = 158
				};

                //bc.Insert<Book>(book);
                //return;
                //var list = bc.Query<Book>();// bc.Set<Book>().Where(b => b.Title.Length > 0).ToList();

                EfDbQuery<Book, string> qry = new EfDbQuery<Book, string>();
                qry.AddFunc(d => d.Title.Contains("J"));
                //qry.AddFunc(c => c.Title.Contains("C"));

			  //  Book b = new Book();

			  //  var func = qry.ToQueryFunc();
			  //  bool flag = func.Compile()(b);
			  //  Console.WriteLine(flag);
				var list = bc.Query(qry);

				foreach (var item in list)
				{
					Console.WriteLine(item.Title);
				}

				//var query =
				//			from b in bc.Books
				//			join c in bc.Categories on 
				//			b.Category.Id equals c.Id into a
				//			from x in a.DefaultIfEmpty()
				//			where x.Name.Contains("计算机")
				//			select b;

				//var query2 = from q in query
				//			 where q.Title.Contains("java")
				//			 select q;

				//var query = bc.Books.Where(b => b.Title.Contains("java"))
				//	.Join(bc.Categories, b => b.Category.Id, c => c.Id, (b, c) => new { B = b, C = c })
				//	.Where(b =>b.C.Name.Contains("计算机"))
				//	.Select(b =>b.B)
				//	;

				//foreach (var item in query)
				//{
				//	Console.WriteLine(item.Title);
				//}

				//Book book = new Book
				//{
				//	Id = Guid.NewGuid().ToString("N"),
				//	Title = "java编程思想",
				//	UnitPrice = 108,
				//	Category = new Category
				//	{
				//		Id = Guid.NewGuid().ToString("N"),
				//		Name = "计算机",
				//		Remark = "计算机编程语言类"
				//	}
				//};

				//bc.Books.Add(book);
				//bc.SaveChanges();
				//var query = from b in bc.Books where b.Title.Contains("C#") orderby b.Title select b;
				//foreach (var book in query.Skip(1).Take(1))
				//{
				//	Console.WriteLine(book.Title);
				//}
			}
		}
	}
}
