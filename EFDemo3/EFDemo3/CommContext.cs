﻿/*
 * Model: 
 * Desctiption: 
 * Author: 陈飞
 * Created: 2016/5/17 17:30:42 
 * Copyright：武汉中科通达高新技术股份有限公司
 */

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Reflection;

namespace EFDemo3
{
    public class CommContext<T> : DbContext where T : class
    {
        public DbModelBuilder ModelBuilder { get; set; }
        //public DbSet<T> Items { get; set; }

        public CommContext()
        {

        }

        public CommContext(string str) : base(str)
        {
            this.Database.Log = new Action<string>((s) =>
            {
                Console.WriteLine(s);
            });
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("CHENF");
            Type t = typeof(EntityTypeConfiguration<>);
            t = t.MakeGenericType(typeof(T));
            dynamic d = Activator.CreateInstance(t);
            modelBuilder.Configurations.Add(d);
            this.ModelBuilder = modelBuilder;
            base.OnModelCreating(modelBuilder);
        }
    }
}
